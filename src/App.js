import React from "react";
import "./App.css";
import { Route, Link } from "react-router-dom";
import { layoutGenerator } from "react-break";
import Sign from "./components/Sign";
import Home from "./components/Home";
import Petition from "./components/Restricted/Petition";
import SignaturePage from "./components/Page";
import About from './components/About';
import Success from './components/Success';

function App() {
  const layout = layoutGenerator({
    mobile: 0,
    phablet: 550,
    tablet: 768,
    desktop: 992
  });

  const OnMobile = layout.is("mobile");
  const OnAtLeastTablet = layout.isAtLeast("tablet");
  const OnAtMostPhablet = layout.isAtMost("phablet");
  const OnDesktop = layout.is("desktop");

  return (
    <div>
      <OnAtLeastTablet></OnAtLeastTablet>
      <OnAtMostPhablet></OnAtMostPhablet>

      <OnMobile className="Mobile">
        <div style={{ width: "90%", margin: "0 auto" }}>
          <header id="head">
            <nav id="nav">
              <Link className="Mlink" to="/">
                Home
              </Link>
              <Link className="Mlink" to="/About">
                About
              </Link>
              <Link className="Mlink" to="/Sign-Petition">
                Sign Petition
              </Link>
              <Link className="Mlink" to="/Signature-Page">
                Signature Page
              </Link>
            </nav>
          </header>
          <Route exact path="/" component={Home} />
          <Route path="/Sign-Petition" component={Sign} />
          <Route path="/Signature-Page" component={SignaturePage} />
          <Route path="/About" component={About} />
          <Route path="/Petition-pdf" component={Petition} />
          <Route path="/Success" component={Success} />
        </div>
      </OnMobile>
      <OnDesktop>
        <div style={{ width: "65%", margin: "0 auto" }}>
          <header id="head">
            <nav id="nav">
              <Link className="link" to="/">
                Home
              </Link>
              <Link className="link" to="/About">
                About
              </Link>
              <Link className="link" to="/Sign-Petition">
                Sign Petition
              </Link>
              <Link className="link" to="/Signature-Page">
                Signature Page
              </Link>
            </nav>
          </header>

          <Route exact path="/" component={Home} />
          <Route path="/Sign-Petition" component={Sign} />
          <Route path="/Signature-Page" component={SignaturePage} />
          <Route path="/About" component={About} />
          <Route path="/Petition-pdf" component={Petition} />
          <Route path="/Success" component={Success} />
        </div>
      </OnDesktop>
    </div>
  );
}

export default App;
