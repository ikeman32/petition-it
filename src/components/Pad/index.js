import React from "react";
import styles from "../../styles.module.css";
import SignaturePad from 'react-signature-canvas';
import { layoutGenerator } from 'react-break';

const layout = layoutGenerator({
  mobile: 0,
  phablet: 550,
  tablet: 768,
  desktop: 992,
});

const OnMobile = layout.is('mobile');
const OnAtLeastTablet = layout.isAtLeast('tablet');
const OnAtMostPhablet = layout.isAtMost('phablet');
const OnDesktop = layout.is('desktop');

class Signature extends React.Component{
  constructor(props){
    super(props);
    this.state = { trimmedDataURL: null };
  }
  
    sigPad = {};
    clear = (e) => {
      e.preventDefault();
      this.sigPad.clear();
    };
    trim = (e) => {
      e.preventDefault();
      
      this.setState({
        trimmedDataURL: this.sigPad.getTrimmedCanvas().toDataURL('image/png')
      });
      
    };
    
  
    render() {
      let { trimmedDataURL} = this.state;
      this.props.data(trimmedDataURL);
      console.log(trimmedDataURL);
      return (
        <div>
          <OnAtLeastTablet></OnAtLeastTablet>
          <OnAtMostPhablet></OnAtMostPhablet>
          <OnMobile>
          <div className={styles.Mcontainer}>
            <div className={styles.MsigContainer}>
              <SignaturePad
                canvasProps={{ className: styles.MsigPad }}
                ref={ref => {
                  this.sigPad = ref;
                }}
              />
            </div>
            <div style={{width: '50%', margin: '0 auto', border: '2px solid black'}}>
              <button id={styles.clear} className={styles.Mbuttons} onClick={this.clear}>
                Clear
              </button>
              <button id={styles.sign} name={'Sign'} className={styles.Mbuttons} onClick={this.trim}>
                {'Sign'}
              </button>
            </div>
            <div>
              <p style={{fontSize: '0.9rem'}}><span style={{fontWeight: 'bold'}}>Instructions</span>: User your mouse or finger to sign the petition then click the Sign button. Your signature will then appear in the Signature input box as encoded text. Click Clear if you want to start over.</p>
            </div>
            {trimmedDataURL ? (
              <img className={styles.sigImage} src={trimmedDataURL} alt={"Signed"} />
            ) : null}
          </div>
          </OnMobile>
          <OnDesktop>
          <div className={styles.container}>
          <div className={styles.sigContainer}>
            <SignaturePad
              canvasProps={{ className: styles.sigPad }}
              ref={ref => {
                this.sigPad = ref;
              }}
            />
          </div>
          <div style={{margin: '0 auto', width: '25%', marginTop: '1rem', padding: '1rem'}}>
            <button className={styles.buttons} onClick={this.clear}>
              Clear
            </button>
            <button name={'Sign'} className={styles.buttons} onClick={this.trim}>
              {'Sign'}
            </button>
          </div>
          <div>
            <p><span style={{fontWeight: 'bold'}}>Instructions</span>: User your mouse or finger to sign the petition then click the Sign button. Your signature will then appear in the Signature input box as encoded text. Click Clear if you want to start over.</p>
          </div>
          {trimmedDataURL ? (
            <img className={styles.sigImage} src={trimmedDataURL} alt={"not working"} />
          ) : null}
          </div>
          </OnDesktop>
        </div>
      );
    }
}

export default Signature;