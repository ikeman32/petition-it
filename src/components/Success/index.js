import React from "react";
import "./styles.css";
import { layoutGenerator } from "react-break";

const layout = layoutGenerator({
  mobile: 0,
  phablet: 550,
  tablet: 768,
  desktop: 992
});

const OnMobile = layout.is("mobile");
const OnAtLeastTablet = layout.isAtLeast("tablet");
const OnAtMostPhablet = layout.isAtMost("phablet");
const OnDesktop = layout.is("desktop");
const Success = () => {
  return (
    <div id="home">
      <OnAtLeastTablet></OnAtLeastTablet>
      <OnAtMostPhablet></OnAtMostPhablet>
      <OnMobile className="Mobile">
        <h1 id="header">Success!</h1>
        <p className="paragraph">
          You have successfully signed the petition. Thank you.
        </p>
      </OnMobile>
      <OnDesktop>
        <h1 id="header">Success!</h1>
        <p className="paragraph">
          You have successfully signed the petition. Thank you.
        </p>
      </OnDesktop>
    </div>
  );
};

export default Success;
