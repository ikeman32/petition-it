import React, { useState } from "react";
import Popup from "reactjs-popup";
import Signature from "../Pad";
import axios from "axios";
import "./styles.css";
import { layoutGenerator } from "react-break";

const layout = layoutGenerator({
  mobile: 0,
  phablet: 550,
  tablet: 768,
  desktop: 992
});

const OnMobile = layout.is("mobile");
const OnAtLeastTablet = layout.isAtLeast("tablet");
const OnAtMostPhablet = layout.isAtMost("phablet");
const OnDesktop = layout.is("desktop");

const Sign = props => {
  const [sig, setSig] = useState("");
  const [signed, setSigned] = useState({
    name: "",
    city: "",
    state: "",
    signature: ""
  });
  const handleClick = Sign => {
    setSig(Sign);
  };
  const focus = e => {
    e.target.click();
  };
  const onChangeHandler = event => {
    event.preventDefault();

    setSigned({
      ...signed,
      [event.target.name]: event.target.value
    });
    
  };

  const submitHandler = e => {
    e.preventDefault();
    const srv = "https://petition-it-be.herokuapp.com/api/petition/sign";
    axios
      .post(srv, { ...signed })
      .then(res => {
        console.log("Petition Signed", res.data);
        setSigned({
          name: "",
          city: "",
          state: "",
          signature: ""
        });
        setSig("");
        props.history.push('/Success');
      })
      .catch(err => {
        console.log(err);
      });
  };

  const popupClose = e => {
    e.preventDefault();
    setSigned({
      ...signed,
      signature: sig
    });
    console.log("signature1", signed);
  };

  return (
    <div>
      <OnAtLeastTablet></OnAtLeastTablet>
      <OnAtMostPhablet></OnAtMostPhablet>
      <OnMobile>
        <div style={{ width: "100%" }}>
          <h1 id="heading">Signature Card</h1>
          <form id="MSignForm" onSubmit={submitHandler}>
            <input
              name="name"
              onChange={onChangeHandler}
              value={signed.name}
              className="Minputs"
              type="text"
              placeholder="Printed Name"
              required
            />
            <input
              name="city"
              onChange={onChangeHandler}
              value={signed.city}
              className="Minputs"
              type="text"
              placeholder="City"
              required
            />
            <input
              name="state"
              onChange={onChangeHandler}
              value={signed.state}
              className="Minputs"
              type="text"
              placeholder="State"
              required
            />
            <Popup
              trigger={
                <input
                  value={sig}
                  onChange={handleClick}
                  name="signature"
                  onFocus={focus}
                  className="Minputs"
                  type="text"
                  placeholder="Signature"
                  required
                />
              }
              modal
              closeOnDocumentClick
              contentStyle={{width: '100%'}}
              onClose={popupClose}
            >
              {close => (
                <div style={{width: '100%'}}>
                  <Signature data={handleClick} />
                  <button className="close" onClick={close}>
                    &times;
                  </button>
                </div>
              )}
            </Popup>
            <div id="Mbtn-div">
              <button type="submit" id="btn">
                Sign Petition
              </button>
            </div>
          </form>
        </div>
      </OnMobile>
      <OnDesktop>
        <div style={{ width: "100%" }}>
          <h1 id="heading">Signature Card</h1>
          <form id="SignForm" onSubmit={submitHandler}>
            <input
              name="name"
              onChange={onChangeHandler}
              value={signed.name}
              className="inputs"
              type="text"
              placeholder="Printed Name"
              required
            />
            <input
              name="city"
              onChange={onChangeHandler}
              value={signed.city}
              className="inputs"
              type="text"
              placeholder="City"
              required
            />
            <input
              name="state"
              onChange={onChangeHandler}
              value={signed.state}
              className="inputs"
              type="text"
              placeholder="State"
              required
            />
            <Popup
              trigger={
                <input
                  value={sig}
                  onChange={handleClick}
                  name="signature"
                  onFocus={focus}
                  className="inputs"
                  type="text"
                  placeholder="Signature"
                  required
                />
              }
              modal
              closeOnDocumentClick
              onClose={popupClose}
            >
              {close => (
                <div>
                  <Signature data={handleClick} />
                  <button className="close" onClick={close}>
                    &times;
                  </button>
                </div>
              )}
            </Popup>
            <div id="btn-div">
              <button type="submit" id="btn">
                Sign Petition
              </button>
            </div>
          </form>
        </div>
      </OnDesktop>
    </div>
  );
};

export default Sign;
