import React from "react";
import Home from "../Home/index";
import { Document, Page, Text, pdf } from "@react-pdf/renderer";
import jsPDF from "jspdf";
import { renderToString } from "react-dom/server";
import html2canvas from "html2canvas";

const Petition = () => {
  const page1 = () => (
    <div>
      <h1>Petition</h1>
      <p>
        We the People of the United States, in order to preserve this Great
        Republic and avoid the the nightmare of Civil War, do hereby exercise
        our Sovereign Right to peaceably petition our government for a redress
        of grievances. Be it know to the powers that be in all levels of
        government, Local, State and Federal hereafter referred to as The
        Government, that this document is not to be construed as a threat of any
        kind, nor as a declaration of intent to secede from the Union, but as a
        stern reminder that their just powers are derived from the consent of
        the governed; and that, We the People have the right, power and duty to
        withdraw our consent whenever such government becomes destructive to the
        purposes for which it was instituted, as was so enshrined in one of our
        most sacred of documents, The Declaration of Independence. As Thomas
        Jefferson so eloquently wrote within this sacred document: 'Prudence,
        indeed, will dictate that Governments long established should not be
        changed for light and transient causes; and accordingly all experience
        hath shewn, that mankind are more disposed to suffer, while evils are
        sufferable, than to right themselves by abolishing the forms to which
        they are accustomed.'
      </p>

      <p>
        So as not to ignite the flames that once drove our founding fathers to
        commit themselves to Treason, We the People hereby petition The
        Government and list our grievances, that they may be addressed with all
        due consideration and preserve our Republic for which many brave souls
        gave their lives to help create. We therefore charge The Government to
        receive this petition and answer to these grievances herein.
      </p>
      <p>The Government, its members, or agents has on one or more occasion:</p>

      <ul>
        <li>Spied on its own people under the premise of national security</li>
        <li>
          In the past, forcibly removed the good citizens from their homes and
          imprisoned them without trial or due process for an indefinite period;
          for no better reason than they were born to the ethnic group of our
          enemies at the time
        </li>
        <li>
          Mismanaged the public funds to such an extent that the public debt is
          unmanageable
        </li>
        <li>
          Ignored the will of the people by attempting to pass legislation
          contrary to the Constitution under the premise of public safety
        </li>
        <li>
          Eroded our Constitutional Rights through legislation under false
          pretenses
        </li>
        <li>
          Behaved in a manner unbecoming their office or station by pointing the
          finger of blame at others or exposing them to the contempt of the
          people
        </li>
        <li>
          Exempted themselves from the laws that they have created for the good
          of the people
        </li>
        <li>
          Failing to take care of those who have served in the Armed Forces by
          under funding the Veterans Administration and failing to take decisive
          action against corrupt administrators
        </li>
        <li>
          Passed laws that have a corrupting influence upon the courts and
          rewards them for rulings that violate the rights of others under the
          color of law
        </li>
        <li>Kept us in a constant state of war without Declaring War</li>
        <li>
          Regulating things at the Federal level that is best left to the States
        </li>
        <li>Abusing eminent domain </li>
        <li>
          Failing to enforce the laws of the land or to address the deficiencies
          in those laws
        </li>
        <li>Adhering to special interests rather than the people</li>
        <li>Failing to address corruption when the People perceive it</li>
      </ul>
    </div>
  );

  const page2 = () => (
    <div>
      <p>
        One grievance not specifically listed above is especially important that
        bears more attention than just a simple listing, the 2nd Amendment.
        Despite many State Constitutions specifically stating that the Right to
        bear arms is an individual right, legislators have consistently passed
        or attempted to pass laws that contrary to the Constitution of the
        United States and even the Constitution of those states which pass or
        attempt to pass such laws. They do this under the premise of the public
        safety while pretending to insist they support the 2nd Amendment, yet
        their actions and public statement contradict themselves.
      </p>

      <p>
        Members or former members of government have even gone as far to call
        for the repeal of the 2nd Amendment: former SCOTUS Justice Stevens
        (2018), former Congressman Major Owens from New York (1990’s), and in
        2019 the Hawaii Senate passed a resolution urging the Congress to
        propose a Constitutional Amendment to 'Clarify or Repeal' the 2nd
        Amendment. It is clear that there are those within The Government that
        are openly hostile toward the 2nd Amendment and they vilify
        organizations that exist to protect that Right. And pay homage to
        organizations that are just as hostile to the 2nd Amendment and those
        that seek to protect it.
      </p>

      <p>
        They (hostile organizations and members of The Government) make remarks
        that they want 'common sense gun laws' or 'that no one wants to take
        away your guns.' Despite the fact that these so-called common sense gun
        laws that are passed are devoid of common sense or the fact that the
        actions and words of others within government and without specifically
        say that they do want to take away our guns. They make arguments that
        the right to bear arms is not an individual right when clearly the
        SCOTUS and several State Constitutions specifically refute this
        ludicrous argument.
      </p>

      <p>
        It is abundantly clear to We the People that, 'the Right of the People
        to keep and bear arms shall not be infringed,' is unambiguous and is no
        way affected or altered by the clause, 'A well regulated militia being
        necessary to the security of a free state.' It is likewise clear that
        despite the lip service offered up by certain members of The Government,
        that there is a clear and present attempt to usurp the 2nd Amendment and
        that there are those that indeed do wish to take away that right.
      </p>

      <p>
        Thus, We the People hereby request and require The Government to
        forthwith address the grievances herein by the most expedient and
        amicable means possible. If this requires a Constitutional Convention to
        resolve some of these grievances, then so be it. We the People love this
        Republic and do not wish to see it crumble under the hubris that now
        plagues it and pray that you do the right thing.
      </p>

      <h1>
        We the People respectfully submit this petition and affix our signatures
        below.
      </h1>
    </div>
  );

  const click = () => {
   
    // const p1 = html2canvas(<page1 />);
    // const p2 = renderToString(<page2 />);
    // const pdf = new jsPDF("p", "pt", "letter");
    // console.log("page1", p1);
    // const specialElementHandlers = {
    //   "#getPDF": function(element, renderer) {
    //     return true;
    //   },
    //   ".controls": function(element, renderer) {
    //     return true;
    //   }
    // };
    // pdf.fromHTML(p1, 40, 40, {
    //   width: 522
    // });

    // pdf.addPage();

    // pdf.fromHTML(p2, 40, 40, {
    //   width: 522
    // });

    // pdf.save("petition");
  };
  return (
    <div>
      <Home />    
      <button onClick={click}>to PDF</button>
    </div>  );
};

export default Petition;
