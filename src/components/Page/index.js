import React, { useState, useEffect } from "react";
import axios from "axios";
import Posts from "./posts";
import Pagination from './Pagination';
import "./styles.css";

const SignaturePage = () => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(25);//To be set when app is finished.

  useEffect(() => {
    const fetchPosts = async (srv = "https://petition-it-be.herokuapp.com/api/petition/") => {
      setLoading(true);
      const res = await axios.get(srv);
      setPosts(res.data);
      setLoading(false);
    };
    fetchPosts();
  }, []);
  //console.log('posts', posts);
  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);

  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
    <div style={{backgroundColor: 'white'}}>
      <Posts posts={currentPosts} loading={loading} />
      <Pagination
        postsPerPage={postsPerPage}
        totalPosts={posts.length}
        paginate={paginate}
      />
    </div>
  );
};

export default SignaturePage;
