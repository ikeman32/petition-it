import React from "react";

const Posts = ({ posts, loading }) => {
  if (loading) {
    return <h2>Loading...</h2>;
  }

  return (
    <table style={{marginTop: '1.5rem'}}>
      <thead>
        <tr>
          <th>Name</th>
          <th>City</th>
          <th>State</th>
          <th>Signature</th>
        </tr>
      </thead>

      {posts.map(sigs => {
            return (
              <tr key={sigs.id}>
                <td>{sigs.name}</td>
                <td>{sigs.city}</td>
                <td>{sigs.state}</td>
                <td className="last">
                  <img className="sig" src={sigs.signature} alt="Signature" />
                </td>
              </tr>
            );
          })}
    </table>
  );
};

export default Posts;
