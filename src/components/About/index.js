import React from "react";
import "./styles.css";
import { layoutGenerator } from "react-break";

const layout = layoutGenerator({
  mobile: 0,
  phablet: 550,
  tablet: 768,
  desktop: 992
});

const OnMobile = layout.is("mobile");
const OnAtLeastTablet = layout.isAtLeast("tablet");
const OnAtMostPhablet = layout.isAtMost("phablet");
const OnDesktop = layout.is("desktop");
const About = () => {
  return (
    <div id="home">
      <OnAtLeastTablet></OnAtLeastTablet>
      <OnAtMostPhablet></OnAtMostPhablet>
      <OnMobile className="Mobile">
        <h1 id="Mheader">About this Petition</h1>
        <p className="Mparagraph">
        This petition was inspired in part by a viral Facebook post that encouraged people to Copy, Paste and send copies to President Trump. The problem with the post was that it was written in an angry and threatening manner, not to mention that it was four pages long. It was decided by that a more professionally worded petition was in order and that “Real Signatures” were needed to make it more impressive. Not only that, but that the petition with the “Real Signatures be hand delivered on paper to the powers that be directly.
        </p>
        <p className="Mparagraph">
        However, the process of collecting “Real Signatures” from real people was until now a tedious and monumental task requiring mobilization of volunteers to go out and get people to sign.  Online petition platforms like change.org do not collect actual signatures but rather personal information and emails. Information that then belongs to change.org which can then be used for other purposes besides the petition being signed, even be sold to third parties.
        </p>
        <p className="Mparagraph">
        What was needed was a way to get the petition out there via the Internet, collect real signatures from real people and then at a later date present a Hard Copy of the Petition with Signatures to the powers that be. Currently there are no platforms that do this, so the organizers of this petition decided to build one from scratch. Thus “Petition-It” was born and is still very much a work in progress. The software behind this platform is not yet ready for public release, but this petition is. Our intentions for this petition is simple, collect as many signatures as possible and then present this petition to the Washington State Legislature and eventually the US Congress itself, maybe even the President.
        </p>
        <p className="Mparagraph">
        To sign click the link above and you will be brought to the signature form. Fill in your name, your city and your state; when you get to the signature field a popup form will present itself where you can use your mouse, finger or stylus to sign your legal signature. You signature is then encoded into a string of text for easy storage on our database. But when the signature page is rendered a real signature is shown. This method allows us to store images as text rather than an image file such as png or jpg which would require much more space than is practical.
        </p>

      </OnMobile>
      <OnDesktop>
        <h1 id="header">About this Petition</h1>
        <p className="paragraph">
        This petition was inspired in part by a viral Facebook post that encouraged people to Copy, Paste and send copies to President Trump. The problem with the post was that it was written in an angry and threatening manner, not to mention that it was four pages long. It was decided by that a more professionally worded petition was in order and that “Real Signatures” were needed to make it more impressive. Not only that, but that the petition with the “Real Signatures be hand delivered on paper to the powers that be directly.
        </p>
        <p className="paragraph">
        However, the process of collecting “Real Signatures” from real people was until now a tedious and monumental task requiring mobilization of volunteers to go out and get people to sign.  Online petition platforms like change.org do not collect actual signatures but rather personal information and emails. Information that then belongs to change.org which can then be used for other purposes besides the petition being signed, even be sold to third parties.
        </p>
        <p className="paragraph">
        What was needed was a way to get the petition out there via the Internet, collect real signatures from real people and then at a later date present a Hard Copy of the Petition with Signatures to the powers that be. Currently there are no platforms that do this, so the organizers of this petition decided to build one from scratch. Thus “Petition-It” was born and is still very much a work in progress. The software behind this platform is not yet ready for public release, but this petition is. Our intentions for this petition is simple, collect as many signatures as possible and then present this petition to the Washington State Legislature and eventually the US Congress itself, maybe even the President.
        </p>
        <p className="paragraph">
        To sign click the link above and you will be brought to the signature form. Fill in your name, your city and your state; when you get to the signature field a popup form will present itself where you can use your mouse, finger or stylus to sign your legal signature. You signature is then encoded into a string of text for easy storage on our database. But when the signature page is rendered a real signature is shown. This method allows us to store images as text rather than an image file such as png or jpg which would require much more space than is practical.
        </p>
      </OnDesktop>
    </div>
  );
};

export default About;
